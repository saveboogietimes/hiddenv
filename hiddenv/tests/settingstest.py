"""Settings test module.

Extra description.

Attributes:
    SOMETHING: something is this
"""

from typing import Dict, List, Optional, Tuple
from uuid import UUID

from typing_extensions import Annotated

from hiddenv.config import EMPTY_NAMESPACE, Config


class SomeConfig2(Config):
    """
    Attributes:  # noqa
        SOMEY:
        OVERRIDE:
    """

    SOMEY: str = ""
    PROP: List[int]
    TUPLE: Tuple[int, str]
    EMPTY_TUPLE: Tuple[()]
    OVERRIDE: int = 1

    @property
    def prop(self):
        """Property testing."""

        return [1]

    @property
    def tuple(self):
        """Tuple property testing."""

        return 2, "as"

    @property
    def empty_tuple(self):
        """Empty tuple property testing."""

        return ()


class SomeConfig(SomeConfig2, namespace=EMPTY_NAMESPACE):
    """Settings related to second option.

    Longer description.

    Attributes:  # noqa
        SOMEX: smx
        THING:
        NULLABLE:
        SS:
    """

    SOMEX: Annotated[int, "SMX"] = 2
    WHAT: str
    THING: Dict[str, int] = {}
    NULLABLE: Optional[str] = None
    SS: SomeConfig2.extend("SS", exclude=("OVERRIDE", ))  # type: ignore

    @property
    def what(self):
        """what?"""

        return f"{self.SOMEX} -- {self.SOMEX}"

    @property
    def tuple(self):
        """Tuple property testing."""

        return super().tuple[0], "asa"

    @staticmethod
    def _process_somex(value):
        return value * 2


class Hello(Config):
    """
    Attributes:  # noqa
        one: something
    """

    HELL: Annotated[UUID, "HEL"]
    TUPLE: Tuple[int, str]
    LIST: List[int]

    @staticmethod
    def _process_hell(value):
        return UUID(value)

    @property
    def tuple(self):
        """tuple test"""
        return 1, ""

    @property
    def list(self):
        """list test"""
        return [1, 2]


__include__ = (SomeConfig, Hello.extend("ABC"))


class AConfig(Config, namespace="AB"):
    """Settings related to AB.

    Attributes:  # noqa
        AAA: a
        QWE: hello
        BBB: b
    """

    AAA: str = "A"
    QWE: int
    BBB: str = "B"

    def _process_aaa(self, value):
        return f"{value} ({self.QWE})"

    def _process_bbb(self, value):
        return f"{value} ({self.QWE})"


class BConfig(Config, namespace="ABB"):
    """Settings related to ABB.

    Attributes:  # noqa
        BBB_BBB:
    """

    BBB_BBB: str = "bee bee bee"


AB: AConfig
CONFIGURATION = "jejee"
_AAAA = "B"
COMMON = "THAT"
TRUTHY = True
SOMETHING: Optional[Dict[str, int]] = None
SOMETHING_OTHER: str = ""
ooo: int  # note: will be ignored as is not a valid setting name
ABB: BConfig
OTHER_SOMETHING: str = ""
OVERRIDE: str
