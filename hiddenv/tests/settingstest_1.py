# pylint: disable = missing-module-docstring
from typing import Dict, List, Tuple

from typing_extensions import Annotated

from hiddenv.environment import JSONDict, JSONList


BBB = 123
COMMON = "THIS"
jee = 2  # pylint: disable = invalid-name
HIDDENV_APP_NAME = "hiddenv.tests"
HELLO: JSONDict
BYE: Annotated[JSONList, "BYE"]
EXTRA: List[int]
TUPLE: Tuple[int, str]
DICT: Dict[int, bool]
