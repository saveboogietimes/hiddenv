"""Tests module."""

import os
from contextlib import suppress
from tempfile import NamedTemporaryFile
from unittest import TestCase


TEST_FILE_CONTENT = """
FOO=foo
BAR=bar
NORMAL=bar
BEFORE_SPACE =bar
AFTER_SPACE= bar
BOTH_SPACE = bar
DOUBLE_QUOTE="bar"
SINGLE_QUOTE='bar'
ESCAPED="escaped\\"bar"
EMPTY=
EMPTY1=$EMPTY

NORMAL_BAR=${NORMAL}bar
NORMAL_EVALUATED=$NORMAL

EVALUATED_COMMENT=$NORMAL ## ''

QUOTE_CONCAT_ESCAPE=\\"quote $FOO\\"



QUOTE_CONCAT='quote $FOO'
QUOTE_ESCAPE_DOLLAR="foo\\${BAR}"
export EXPORTED_BAR=bar
lowercase_bar=bar

""" + "\t  " + """


lowercase_bar_comments=bar # this is foo

CONCATENATED_BAR_BAZ_COMMENTED="bar#baz" # comment""" + " " + """



# HERE GOES FOO


#foo="ba#r"


# wrong
# lol$wut

TEST_DICT=" a=3;b=asd ;"

"""

TEST_FILE_NAME = "test.env"
DOTENV_EXPECTED = dict(
    FOO="foo",
    BAR="bar",
    NORMAL="bar",
    BEFORE_SPACE="bar",
    AFTER_SPACE="bar",
    BOTH_SPACE="bar",
    DOUBLE_QUOTE="bar",
    SINGLE_QUOTE="bar",
    ESCAPED="escaped\"bar",
    EMPTY="",
    EMPTY1="",
    NORMAL_BAR="barbar",
    NORMAL_EVALUATED="bar",
    EVALUATED_COMMENT="bar",
    QUOTE_CONCAT_ESCAPE="\"quote foo\"",
    QUOTE_CONCAT="quote foo",
    QUOTE_ESCAPE_DOLLAR="foo${BAR}",
    EXPORTED_BAR="bar",
    lowercase_bar="bar",
    lowercase_bar_comments="bar",
    CONCATENATED_BAR_BAZ_COMMENTED="bar#baz",
    TEST_DICT=" a=3;b=asd ;",
)


def temporary_dotenv_file(dotenv_string):
    """Creates a temporary file for dotenv content."""

    file_descriptor = NamedTemporaryFile()  # pylint: disable = consider-using-with
    file_descriptor.write(dotenv_string.encode("utf-8"))
    file_descriptor.seek(0)
    return file_descriptor


class BaseTestCase(TestCase):
    """Tests for hiddenv.dotenv and hiddenv.environment."""

    test_file_name = TEST_FILE_NAME
    test_file_content = TEST_FILE_CONTENT
    expected = DOTENV_EXPECTED

    @classmethod
    def tearDownClass(cls) -> None:
        """On teardown, removes written dotenv file if it exists."""

        with suppress(FileNotFoundError):
            os.remove(cls.test_file_name)
