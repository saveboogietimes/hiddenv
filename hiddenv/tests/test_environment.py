"""Tests for hiddenv.environment."""

import os

from hiddenv.environment import Env, empty
from hiddenv.tests import BaseTestCase


class TestEnv(BaseTestCase):
    """Tests for hiddenv.environment."""

    def test_empty(self):
        """Tests stringification of `empty`."""

        self.assertEqual(str(empty), "EMPTY")

    def test_env_load_fail(self):
        """Tests for expected failure when attempting to loading non-existent dotenv file."""

        with self.assertRaises(FileNotFoundError):
            Env.load(filename=self.test_file_name)

    def test_env_load(self):
        """Tests loading values to as environment variables from dotenv file."""

        with open(self.test_file_name, "w") as f:
            f.write(self.test_file_content)
        env = Env.load(filename=self.test_file_name)
        self.assertEqual({
            "a": "3",
            "b": "asd"
        }, env.dict("TEST_DICT"))
        for key, value in self.expected.items():
            self.assertEqual(value, env.str(key))
        os.remove(self.test_file_name)

    def test_env_read(self):
        """Tests reading and casting from environment variables."""

        os.environ.update(STR="hello")
        env = Env(
            STR="text",
            BOOL="True",
            NUM="7",
            JSON='{"one": true}',
            LIST="one,two, three",
            TUPLE="(one, two ,three)",
            DICT=" a=3;b=asd ;"
        )
        self.assertEqual("hello", env.str("STR"))
        self.assertIs(True, env.bool("BOOL"))
        self.assertIs(7, env.int("NUM"))
        self.assertEqual(7.0, env.float("NUM"))
        self.assertIsInstance(env.float("NUM"), float)
        self.assertDictEqual(dict(one=True), env.json("JSON"))
        self.assertListEqual(["one", "two", "three"], env.list("LIST"))
        self.assertTupleEqual(("one", "two", "three"), env.tuple("TUPLE"))
        self.assertDictEqual(dict(a="3", b="asd"), env.dict("DICT"))
        with self.assertRaises(LookupError):
            env.str("DOES_NOT_EXIST")
        self.assertEqual(env.str("DOES_NOT_EXIST", default="Nope"), "Nope")
